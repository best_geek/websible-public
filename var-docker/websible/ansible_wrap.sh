#!/usr/bin/env bash

SSH_KEY="/var/websible/.auth-key"
PLAYBOOKS="/var/playbooks"
CALL_LOG="called.log"
PIDTRACKER="/var/websible/runs/pids"
MAX_PLAYS=$(cat /var/websible/maxplays)
ESCAPE_CMD="/var/websible/escape.sh"
#$ANSIBLE_U gets inherited from docker-compose.yaml

echo "This script was called on $(date)" >> $CALL_LOG

ESCAPE_RESULT=$($bash $ESCAPE_CMD "$@")
if (echo $ESCAPE_RESULT | grep "STOP!")
then
	echo "I see you doing the naughty"
	echo "Stop that shit please"
	exit 1
fi


#get sysargs
for i in "$@"
do
case "$i" in
    -p=*|--playbook=*)
    PLAYBOOK="${i#*=}"
    shift # past argument=value
    ;;
    -rl=*|--run-log=*)
    RUNLOG="${i#*=}"
    shift # past argument=value
    ;;
    -s=*|--stop-playbook=*)
    STOPPLAYBOOK="${i#*=}"
    shift # past argument=value
    ;;

esac
done




if [ -z "$PLAYBOOK" ]
then
	echo "you did not specify a playbook"
	exit 1
fi



###################################################
#     Continue onto run playbook as default       #
###################################################


#see if keyfile
if [ ! -f $SSH_KEY ]; then
	echo "[FATAL] Auth key not found"
	echo "Did you put one in the 'ssh_key' directory?"
	echo "Tried to run playbook but with no key present" >> /var/websible/websible.log
	exit 1
fi



#work out if running
N_RUN=$(ls /var/websible/runs/ | grep $PLAYBOOK | wc -l)

if [ "$N_RUN" -lt "$MAX_PLAYS" ]
then

	if [ ! -f "$PLAYBOOKS"/"$PLAYBOOK" ]; then
		echo "Your playbook was not found"
		ps aux | grep $RUNLOG | grep 'tail -f'| grep 'timeout' | awk '{print $2}' | xargs kill > /tmp/processes.txt
		exit 1
	fi


	###### runtime

	export ANSIBLE_HOST_KEY_CHECKING=False
	echo "Running: $PLAYBOOK"
	echo "There are: $N_RUN copies of this playbook running"

	echo "Remote user called playbook and running with: $@" >> /var/websible/websible.log
	echo $$ $PLAYBOOK $RUNLOG>> $PIDTRACKER
	/usr/bin/ansible-playbook --user "$(cat /var/websible/ansible_u)" --key-file "$SSH_KEY" "$PLAYBOOKS"/"$PLAYBOOK"


	if [ ! -z "$RUNLOG" ]
	then
		#kill any associated processes with the tail initiated in runplay.php. Stops browser from contantly loading
		#first line getd actual process, to ignore our grep cmd in ps output
		#ps second column returns PI
		#we grep for timeout because that can be used to uniquely identify process php called
		#then pass to xargs to kill

                sleep 5 #works as a hack to give ansible playbook  enough time to stop

                #echo "$RUNLOG" > /tmp/test.txt
                ps aux | grep $RUNLOG | grep 'tail -f'| grep 'timeout' | awk '{print $2}' | xargs kill > /tmp/processes.txt

		rm $RUNLOG


		#remove pid from pid tracker
		PROCID=$$
		sed -i "/$PROCID /d" $PIDTRACKER 



	fi
else

	#output error because failed running number check
	echo "There are too many processes running for $PLAYBOOK. You may wish to increase MAX_PLAYS in docker-compose.yaml \
if you are constantly recieving this message as it is dependant on the amount of hosts you have"


	exit 1
fi
