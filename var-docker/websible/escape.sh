#!/bin/bash

#simple test to escape special chars


RUNWITH="$@"


#note, have removed / as this is can be where logfile lives
if (echo $RUNWITH | grep -q "[]:?#@\!\$&'()*+,;%[]")
then
	echo "STOP!"
	echo "Remote user called wrapper with $RUNWITH and was stopped" >> /var/websible/websible.log
fi
