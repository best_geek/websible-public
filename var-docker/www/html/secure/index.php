<!DOCTYPE html>
<html>
<head>
<title>Websible</title>
<link rel="stylesheet" href="../style.css">
</head>



<script src="https://code.jquery.com/jquery-latest.js"></script>
<script>
    $(document).ready(function(){
                 $("#refresh").load("index.php #refresh");
        setInterval(function() {
            $("#refresh").load("index.php #refresh");
        }, 3000);
    });
 
</script>




<?php
//loc to store any vars
$playbookdir = "/var/playbooks";
$logfile = "/var/websible/runs/";
?>

<! --- //navbar --->
<a href="index.php"><img src="../res/Logo.png" style="width:220px"></a>
<hr>
<br>
<!--- //navbar end --->

<h1>Playbooks</h1>
<br>
<h2> Available Playbooks </h2>
<br>
<div id="refresh">
<div class="plist">
<?php
//php function to list files in a dir
if ($handle = opendir($playbookdir))
{

        //begin table
        echo '<table>';
        echo '<tr>';
        echo '<th>Playbook</th>';
        echo '<th style="text-align:center;">Running:</th>';
        echo '<th>Action</th>';
        echo '</tr>';
        while (false !== ($file = readdir($handle)))
        {
                if ($file != "." && $file != "..")
                {

                        //look for .yaml in files
                        if (strpos($file, '.yaml') !== false || strpos($file, '.yml') !== false)
                        {
                                if (strpos($file, ' ') === false)
                                {
                                        //creates a hyperlink of each filename
                                        //sends to playbook handler
                                        echo '<tr>';
                                        echo '<td style="font-family: \'Open Sans\', sans-serif;font-size;0.6em;">' . $file . '</td>';
                                        $running = shell_exec("cat /var/websible/runs/pids | grep $file.| wc -l");
                                        echo '<td style="text-align:center;font-family: \'Roboto Mono\', monospace;">' . $running . '</td>';
                                        echo '<td><a style="text-align:center;font-family: inherit;"href="runplay.php?runplay=' . $file . '"> [RUN]</a></td>';

                                        echo '</tr>';
                                }
                        }
                }
        }
        closedir($handle); //close the handle so other apps can use
        echo '</table>';
}
?>
<br>
<p style="font-size:0.8em;">
- All files not having the correct yaml extension will not show
<br>
- All filenames containing spaces are invalid 
<br>
- Playbooks are synced from the mapped volume every 5 minutes
<br>
- <?php echo shell_exec("tail -n 1 /var/websible/websible_cron.log");?> 
</p>
<br>




<!--- all running section --->
<?php
//loc to store any vars
$pidfile = "/var/websible/runs/pids";
?>


<br>
<h1>Running Playbooks</h1>
<br>
<h2>All active</h2>

<div class="plist">
<?php
$handle = fopen($pidfile, "r");
if ($handle)
{
        //begin table
        echo '<table>';
        echo '<tr>';
        echo '<th>Playbook</th>';
        echo '<th>Started</th>';
        echo '</tr>';

        while (($line = fgets($handle)) !== false)
        {
                //echo $line;
                $awk = explode(" ", $line);
                //log looks like <pid> <playbook> <log loc>
                $play = $awk[1];
                $log = $awk[2];
                $procid = $awk[0];

                //log ends in time
                $awk = explode(".", $log);
                $logtime = end($awk);

                //get shortlog, get last / for filename
                $awk = explode("/", $log);
                $shortlog = end($awk);

                echo "<tr>";
                echo "<td>$play</td>";
                echo "<td>$logtime</td>";

                echo "</tr>";

        }
        echo "</table>";
        fclose($handle);
}
else
{
        echo "error opening file";
        // error opening the file.
        
}

?>


</div>

</p>
</div>
</

<br><br><br>
<p style="font-size:0.8em;"><a href="debug.php">APPLICATION INFO</a></p>

</body>
</html>

