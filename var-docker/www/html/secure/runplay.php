<!DOCTYPE html>
<html>
<head>
<title>Playbook Run</title>
<link rel="stylesheet" href="../style.css">
</head>



<?php
//loc to store any vars
$playbook_wrap = "/bin/bash /var/websible/ansible_wrap.sh";

?>
<! --- //navbar --->
<a href="index.php"><img src="../res/Logo.png" style="width:220px"></a>
<hr>
<br>
<!--- //navbar end --->




<!--- //jsquery to autoscroll --->
<script src="https://code.jquery.com/jquery-latest.js"></script>
<script>
//set default
var scrolled = false;

$( document ).ready(function() {
    // this works because php spawns a tail process which stop the page from 
    // fully loading. When playbook is done, then ansible_wrap kills the tail
    // resulting in document.ready and browser then stops auto scroll
    console.log( "Stopped autoscroll" );
    scrolled=true;
});


$(window).bind('mousewheel', function(event) {
    if (event.originalEvent.wheelDelta >= 0) {
        console.log('Scroll up');
	scrolled=true;   
	//set var to stop scroll
       }
});


//autoscroll every 2 seconds unless user scrolls up
setInterval(function(){ 
  if(!scrolled){
	//smooth scroll
	$("#scrollme").animate({ scrollTop: $("#scrollme")[0].scrollHeight}, 1000);
}
 }, 1000);

</script>




<h1>Playbook result</h1>
<br>
<div id="scrollme" class="codebox" style="width:800px;">
<p>
<?php
if ($_GET['runplay'])
{
        $playbook = $_GET['runplay'];
        //$playbook=escapeshellcmd($playbook);
        $playbook = escapeshellarg($playbook);
        $now = date('h-i-sa');
        $logfile = "/var/websible/runs/" . $playbook . "." . $now;
        echo shell_exec("/var/websible/ansible_wrap.sh --playbook=$playbook --run-log=$logfile 2>&1 | tee -a $logfile 2>/dev/null >/dev/null &");
        //fun fact: has to be "" in shell exec to pass args
        //echo shell_exec("tail -f $logfile");
        //use timeout as a backup mechanism for closing process so no hang
        //ansible_wrap should now also handle this
        $handle = popen("timeout 600 tail -f $logfile", 'r');
        while (!feof($handle))
        {
                $buffer = fgets($handle);
                echo "$buffer<br/>\n";
                ob_flush();
                flush();
        }
        pclose($handle);

}
?>
</p>
</div>
<br><br>
</body>
</html>

