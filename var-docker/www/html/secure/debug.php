<!DOCTYPE html>
<html>
<head>
<title>Websible</title>
<link rel="stylesheet" href="../style.css">
</head>


<?php
//loc to store any vars
$playbookdir = "/var/playbooks";
$logfile = "/var/websible/runs/";
?>


<! --- //navbar --->
<img src="../res/Logo.png" style="width:220px">
<hr>
<div class="topnav">
        <!--- bttn --->
        <a href="index.php">Home</a>

</div>
<hr>
<br>
<!--- //navbar end --->

<h2>Running config</h2>
<div class="codebox">
<p>
<!-- Runtime information. -->
<?php
echo 'Current user: ' . shell_exec('whoami');
?>
<br>

<?php
echo 'Running on: ' . shell_exec('hostname');
?>
<br>

<?php
echo 'OS: ' . shell_exec('lsb_release -d');
?>
<br>

<?php
echo 'Ansible version: ' . shell_exec('ansible-playbook --version | head -n 1');
?>
<br>

<?php
echo 'CPU load: ' . shell_exec('cat /proc/loadavg');
?>
<br>


<?php
echo 'CPU load: ' . shell_exec('free -m | head -n 2');
?>
<br>

<?php
echo 'Current PHP version: ' . phpversion();
?>
<br>

<?php
echo 'Date:' . shell_exec('date');
?>
</p>
</div>

</body>
</html>

