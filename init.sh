#!/bin/bash

### MAPPED PLAYBOOK CHECK ###
if [ -d "/tmp/playbooks" ] 
then
echo "Detected the mapped location for playbooks. Copying" 
cp -r /tmp/playbooks/* /var/playbooks/
fi
###

#run installation playbook
ansible-playbook /tmp/controlplays/first_run.yaml

### START SERVICES ###

#start apache2
echo "starting services"
a2ensite website-ssl
a2enmod ssl
a2enmod $PHP_V
/etc/init.d/apache2 start

#start cron 
echo "start cron"
cron -f
#easy hack to leave running
tail -f /dev/null
###
