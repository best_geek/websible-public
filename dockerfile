FROM debian:stable-slim

ENV PHP_V=php7.3

#install apache dependencies and packages
RUN apt update -y && \
apt install apache2 \
apache2-utils \ 
$PHP_V \
libapache2-mod-$PHP_V \
ansible \
curl \
cron \
ssh -y 

#add required folder
RUN mkdir /var/playbooks

#copy over build files
COPY ./var-docker/www/html /var/www/html
COPY ./etc-docker/apache2/sites-available/website-ssl.conf /etc/apache2/sites-available/website-ssl.conf

#cop SSL certs
COPY ./certs/*.pem /etc/ssl/private/website.pem
COPY ./certs/*.key /etc/ssl/private/website.key

#copy  app file
COPY ./ssh-key/* /var/websible/.auth-key
COPY ./var-docker/websible /var/websible

#COPY Cron 
COPY ./etc-docker/cron.d/websible_cron /etc/cron.d/websible-cron

#copy working scripts
COPY ./controlplays/* /tmp/controlplays/
COPY healthcheck.sh healthcheck.sh
COPY init.sh init.sh


