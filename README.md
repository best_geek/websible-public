# Websible
A docker container to run playbooks with a web UI.
The main reason this whole project existed is because this is pretty much exclusively  all I wanted to do from a web browser insted of having to hop through to run playbooks on a CLI.

# Getting Started
This will take you through the setup process. Please note these steps should be followed before building the docker.

You should already have the basics installed: docker and docker-compose


## Environment variables
The docker has a compose file which contains some important environment variables.

 - `ANSIBLE_U` This should be set to the user that you'd use normally for your playbook authentication via SSH
 - `MAX_PLAYS` This is how many of the same playbook can run at any given time. If you only wanted to be able to run one copy of each playbook at a time, set this to '1'

## Mapped  volumes
**Playbooks**
The docker has a compose requires access to playbooks to run. This is mapped inside the container to `/tmp/playbooks`

You'll see this mapped in the `docker-compose` with something similar:
  `#INSERT_PLAYBOOK_DIR_HERE#:/tmp/playbooks:ro`

By default, this is mapped to a directory in this repository which is: `var-docker/playbooks`. Either copy your playbooks into here or map to an existing location.

If you map to an existing playbook elsewhere:
 - Playbooks are read as read only so Websible cannot ruin them in any way
 - They are synced every 5 minutes, so please allow 5 minutes for them to appear in the webUI
 - The user which you create the container under **must** also have access to these playbooks



**Ansible config:**

The ansible config path is mapped as a volume. By default, this is mapped to a folder in this dir, `etc-docker/ansible`:
This is done in the `docker-compose.yaml` in the followling line:

`-./etc-docker/ansible:/etc/ansible:ro`

You can sync to another folder or even config files that you may already have in `/etc/` on the host. If you do this, make sure your user has adequate permissions to read those files.

The config is mapped as read-only so it cannot ever be changed within the container and Websible will not break any config files.

## SSH Keyfile
You should place your SSH keyfile for accessing other hosts in the folder `ssh-key`. Websible only supports authentication via public key. Each  destination host should have this key file in their respective authorized_keys file.

## SSL Certs
The docker uses HTTPs so that credentials and all information is passed through an encrtyped channel. These need to generated before your first run. You can do this with the following (provided you are in the root of the repository)

`cd certs; ./create-certs.sh ; cd -`

## Running
You are now ready to start building the container. Please note, this process takes time while docker creates the image. From the root of the repository, execute:

`docker-compose build`

Once this has completed successfully bring the container up with

`docker-compose up -d`

This will take ~1minute for it to come up fully. You will know when it has by running:

`docker ps --filter "name=websible"`

... and seeing that the container is healthy under the status column:

`Up 2 minutes (healthy)`

It may show `Up 10 seconds (health: starting)`, in which case give it more time to come up.

**Add a user to sign in via the web UI**
Now the container is up, we need to add a user to be able to sign in. From the root of the repository, run the following and fill in the prompts:

`./add_user.sh`

(user auth files are stored in a volume and are persistant if you stop the container or remove it)

**YOU ARE NOW UP AND RUNNING!**

## Usage
Now you've got everything up and running, navigate to `https://IP_OF_HOST` and sign in.

Click on `[RUN]` to run a playbook.
The UI will show all playbooks initiated through the webUI and how many of each playbook are running.

As mentioned above, by default if you need to run more than 1 instance of the same playbook, you can do this by editing `MAX_PLAYS` in `docker-compose.yaml` . The default, 4, is usually more than enough.

The most CPU intensive part is the actual running of the playbooks. If you have many playbooks running this could cause performance degradation. Clicking on 'APPLICATION INFO' on the home page should give you suffcient information to determine if the system is heavily loaded.

## Useful
There are two experimental variables:

  - `PHP_V` - in the event you wish to change the version. Currently this is set to `php7.3`. Please note, if you change this in `docker-compose.yaml` you must also change this in `dockerfile` to match . I make no guarantees that future versions of PHP will work.

  - `RUN_U` - **This should not be changed unless you have an edge usecase for it.** This the user that everything runs under within the container. Please note, if you change this you will also need to go into `etc-docker/cron.d/websible_cron` and change this section: `chown -R websible:websible` to match the user you changed to.




## DNS issues fix
A lot of my playbooks rely on using DNS name resolution for devices on the local network. I have discovered an issue when running pihole in a docker on the same host. The fix involves editing config files for pihole as well as for docker. 

### Pihole:
In the docker-compose for pihole there is a section for dnsmasq. By default this is usually in the same dir as the docker-compose file. For me, this is the case. 

The folder name I have for this is ```etc-dnsmasq.d```

Go into this folder and create a new file named ```99-my-settings.conf```

In the file add the following, making note of replacing the IP of the host running pihole

```
listen-address=<IP OF DOCKER HOST>
```

**Edit docker-compose.yaml**

There is a further enviroment variable to add into the ```docker-compose.yaml``` file. 

```ServerIP: <INSERT IP OF DOCKER HOST>```

### Websible:

To get websible to use pihole as a local DNS resolve, you can add the ```DNS``` option in the compose file followed by the IP of your host with the pihole container. 

Alternatively, as an easier fix, get all docker containers to recognise pihole add the following in ```etc/docker/daemon.json.``` (create if not present):

```
{
    "dns": ["<IP OF DOCKER HOST FOR PIHOLE>", "8.8.8.8"]
}
```

Restart docker with
```sudo systemctl restart docker```

Now all your dockers respect the pihole as a DNS resolver. 