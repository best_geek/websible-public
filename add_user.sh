#!/bin/bash
DOCKER_N="websible"

printf "Please be aware if you've changed the docker name \
you will need to update the DOCKER_N var in this file\n"

printf "Please enter a user to update or create a password for: "
read U_NAME

printf "\nSetting up user for $U_NAME on $DOCKER_N\n"
docker exec -it $DOCKER_N htpasswd -c /var/www/auth/.htpasswd $U_NAME
